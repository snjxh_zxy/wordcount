#include<iostream>
#include<stdio.h>
#include<ctype.h>//调用isalnum函数 
#include<cstring> //strmp函数不在C++的标准库中，在C的标准库中 
//自定义函数 
using namespace std;
void CountChar(FILE *file)//统计字符数量 
{
    char a;
	int CountChar=0;
    a=fgetc(file);//fegtc用于读取字符数，读一个自动到下一个 
    while(!feof(file))// feof判断文件是否结束 
    {
        CountChar++;
        a=fgetc(file);
    }
    cout<<"字符数:"<<CountChar<<endl;
    rewind(file);//读写位置指针重置到文件开头 
}

void CountWord(FILE *file)//统计单词数量 
{
    char b;
    int CountWord=0;
    b=fgetc(file);
    while(!feof(file))
    {
        if(isalnum(b))//该函数检测字符串的字母和数字 
        {
            while(isalnum(b))
            {
                b=fgetc(file);
            }
            CountWord++;
        }

        b=fgetc(file);
    }
    cout<<"单词数:"<<CountWord<<endl;
    rewind(file);
}

void CountLine(FILE *file)//统计句子数 
{
    
	char c;
	int CountLine=0;
    c=fgetc(file);
    while(!feof(file))
    {
        if(c=='.'||c=='!'||c=='?')
        {
            CountLine++;
        }
        c=fgetc(file);    
    }
    cout<<"句子数:"<<CountLine<<endl;
    rewind(file);
}

int main(int argc, char* argv[])//输入不同数量的参数 
{
    FILE *file;
    switch(argc)
    {
    
        case 3:
            file=fopen(argv[2],"r");
            if(file==NULL)
            {
                cout<<"文件错误"<<endl;
                exit(0);//正常退出 
            }
            if(strcmp(argv[1],"-c")==0)//利用字符串比较函数来选择输入 
            {
                CountChar(file);
            }
            else if(strcmp(argv[1],"-w")==0)
            {
                CountWord(file);
            }
            else if(strcmp(argv[1],"-l")==0)
            {
                CountLine(file);
            }
           
            else
            {
                cout<<"格式错误"<<endl;
            }
            break;
        case 4:
            file=fopen(argv[3],"r");
            if(file==NULL)
            {
                cout<<"文件错误"<<endl;
                exit(0);
            }
            if(strcmp(argv[1],"-c")==0)
            {
                if(strcmp(argv[2],"-w")==0)
                {
                    CountChar(file);
                    CountWord(file);
                }
                else if(strcmp(argv[2],"-l")==0)
                {
                    CountChar(file);
                    CountLine(file);
                }
                else
                {
                    cout<<"格式错误"<<endl;
                }
            }
            else if(strcmp(argv[1],"-w")==0&&
                    strcmp(argv[2],"-l")==0)
            {
                CountWord(file);
                CountLine(file);
            }
            else
            {
                cout<<"格式错误"<<endl;
            }
            break;
            case 5:
            file=fopen(argv[4],"r");
            if(file==NULL)
            {
                printf("文件错误");
                exit(0);
            }
            
            if((strcmp(argv[1],"-c")==0)&&
               (strcmp(argv[2],"-w")==0)&&
               (strcmp(argv[3],"-l")==0))
            {
                CountChar(file);
                CountWord(file);
                CountLine(file);
            }
            
            else
            {
               cout<<"格式错误"<<endl;
            }
            break;
        default:
            {
                cout<<"参数个数错误"<<endl;
                exit(0);
            }
    }
    return 0;
}
